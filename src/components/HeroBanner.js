import React from "react";
import { Box, Stack, Typography } from "@mui/material";

import HeroBannerImage from "../assets/images/banner.png";

const HeroBanner = () => (
  <Box sx={{ ml: { sm: "50px" } }} position="relative" p="20px">
    <Typography
      marginTop="24px"
      color="#f7931e"
      fontWeight="600"
      fontSize="70px"
    >
      Fitness Club
    </Typography>
    <Typography
      fontWeight={700}
      sx={{ fontSize: { lg: "44px", xs: "40px" } }}
      mb="23px"
      mt="30px"
      color="white"
    >
      Sweat, Smile <br />
      And Repeat
    </Typography>
    <Typography
      fontSize="22px"
      fontFamily="Alegreya"
      lineHeight="35px"
      color="white"
    >
      Check out the most effective exercises personalized to you
    </Typography>
    <Stack>
      <a
        href="#exercises"
        style={{
          marginTop: "45px",
          fontWeight: "bold",
          textDecoration: "none",
          width: "200px",
          textAlign: "center",
          background: "#db630c",
          padding: "14px",
          fontSize: "24px",
          color: "black",
          borderRadius: "4px",
          fontFamily: "fantasy",
        }}
      >
        Explore Exercises
      </a>
    </Stack>
    <Typography
      fontWeight={600}
      color="#f7931e"
      sx={{
        opacity: "0.1",
        display: { lg: "block", xs: "none" },
        fontSize: "200px",
      }}
    >
      Exercise
    </Typography>
    <img src={HeroBannerImage} alt="hero-banner" className="hero-banner-img" />
  </Box>
);

export default HeroBanner;
