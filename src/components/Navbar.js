import React from "react";
import { Stack } from "@mui/material";
import logo from "../assets/images/Logo.png";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <Stack
      direction="row"
      sx={{
        gap: { sm: "122px", xs: "40px" },
        margin: { xs: "13", sm: "17px" },
        px: "20px",
      }}
    >
      <Link to="/">
        <img
          src={logo}
          alt="logo"
          style={{
            width: "70px",
            height: "48px",
            marginRight: "20px",
            marginTop: "10px",
          }}
        />
      </Link>
      <Stack direction="row" gap="40px" fontSize="24px" alignItems="flex-end">
        <Link
          to="/"
          style={{
            textDecoration: "none",
            borderBottom: "3px solid #f7931e",
            color: "white",
          }}
        >
          Home
        </Link>
        <a href="#exercises" style={{ textDecoration: "none", color: "white" }}>
          Exercises
        </a>
      </Stack>
    </Stack>
  );
};

export default Navbar;
