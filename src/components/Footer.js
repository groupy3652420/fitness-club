import React from "react";
import { Box, Stack, Typography } from "@mui/material";
import Logo from "../assets/images/Logo.png";
const Footer = () => {
  return (
    <Box mt="50px" bgcolor="#000">
      <Stack gap="40px" alignItems="center" px="40px" py="24px">
        <Box display="flex" alignItems="center" gap="8px">
          <img src={Logo} alt="logo" width="55px" height="40px" />
          <Typography
            variant="h5"
            textTransform="uppercase"
            fontWeight="bold"
            color="orange"
          >
            Fitness <span style={{ color: "white" }}>club</span>
          </Typography>
        </Box>

        <Typography color="#afa2a2">Made with love by Joelle</Typography>
      </Stack>
    </Box>
  );
};

export default Footer;
