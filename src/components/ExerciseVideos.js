import { Box, Stack, Typography } from "@mui/material";
import React from "react";
import Loader from "./Loader";

const ExerciseVideos = ({ exerciseVideos, name }) => {
  if (!exerciseVideos) return <Loader />;

  return (
    <Box sx={{ mt: { lg: "100px", xs: "20px" }, p: "20px" }}>
      <Typography
        fontWeight="bold"
        mb="33px"
        color="white"
        sx={{ fontSize: { xs: "2rem", md: "2.5" } }}
      >
        Watch{" "}
        <span style={{ color: "#ff7800", textTransform: "capitalize" }}>
          {name}
        </span>{" "}
        exercise videos
      </Typography>
      <Stack
        justifyContent="flex-start"
        flexWrap="wrap"
        alignItems="center"
        sx={{
          flexDirection: { lg: "row" },
          gap: { lg: "70px", xs: "56px" },
        }}
      >
        {exerciseVideos?.slice(0, 3).map((item, index) => (
          <a
            key={index}
            className="exercise-video"
            href={`https://www.youtube.com/watch?v=${item.video.videoId}`}
            target="_blank"
            rel="noreferrer"
          >
            <img src={item.video.thumbnails[0].url} alt={item.video.title} />
            <Box paddingLeft="0.8rem">
              <Typography variant="h6" color="#918f8f">
                {item.video.title}
              </Typography>
              <Typography variant="h6" fontWeight="bold" color="#444141">
                {item.video.channelName}
              </Typography>
            </Box>
          </a>
        ))}
      </Stack>
    </Box>
  );
};

export default ExerciseVideos;
