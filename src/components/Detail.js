import React from "react";
import { Box, Button, Stack, Typography } from "@mui/material";
import BodyPertImg from "../assets/icons/body-part.png";
import TargetImg from "../assets/icons/target.png";
import EquipmentImg from "../assets/icons/equipment.png";

const Detail = ({ exerciseDetail }) => {
  const { bodyPart, gifUrl, name, target, equipment } = exerciseDetail;

  const extraDetail = [
    {
      icon: BodyPertImg,
      text: bodyPart,
    },
    {
      icon: TargetImg,
      text: target,
    },
    {
      icon: EquipmentImg,
      text: equipment,
    },
  ];
  return (
    <Stack
      bgcolor="black"
      gap="60px"
      sx={{ flexDirection: { lg: "row" }, p: "20px", alignItems: "center" }}
    >
      <img src={gifUrl} alt={name} loading="lazy" />
      <Stack sx={{ gap: { xs: "10px", lg: "10px" } }}>
        <Typography variant="h3" fontWeight="bold" color="#ff7800">
          {name}
        </Typography>
        <Typography variant="h6" color="white">
          Exercises keep you strong. {name} is one of the best exercises to
          target your {bodyPart}. it will help you improve your mood and gain
          energy.
        </Typography>
        {extraDetail?.map((detail) => (
          <Stack
            key={detail.text}
            direction="row"
            alignItems="center"
            color="black"
            gap="24px"
            fontSize="22px"
            fontWeight="semi-bold"
          >
            <Button
              sx={{
                background: "#fff2db",
                borderRadius: "50%",
                width: "60px",
                height: "60px",
              }}
            >
              <img
                src={detail.icon}
                alt={bodyPart}
                style={{ width: "40px", height: "40px" }}
              />
            </Button>
            <Typography
              variant="h5"
              textTransform="capitalize"
              color="#afa2a2"
              fontWeight="bold"
            >
              {detail.text}
            </Typography>
          </Stack>
        ))}
      </Stack>
    </Stack>
  );
};

export default Detail;
